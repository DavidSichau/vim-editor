" This must be first, because it changes other options as side effect
set nocompatible
"Pathogen infection turned on
call pathogen#infect()
call pathogen#helptags()
"syntax highlighting on
syntax on
"opens the file with the intended module
filetype plugin indent on
"no backup file generated
set nobackup
set noswapfile

"execute current script with :R does not need the permissions as it
"automatically detects the interpreter and use the interpreter to execute
"the script see http://superuser.com/a/21503/177041
au BufEnter * if match(getline(1),'^\#!') == 0|
            \execute("let b:interpreter = getline(1)[3:]")|
            \endif

fun! CallInterpreter()
    if exists("b:interpreter")
        exec ("!".b:interpreter." %")
    endif
endfun

"define the new command to execute the interpreter with this script
command! Run :call CallInterpreter()
"map the script run to leader r
:map <Leader>r :Run

"set the current file always as the current dir
set autochdir
"deactivate arrow keys:
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
"unicode
set encoding=utf-8
"change color scheme
colorscheme ansi_blows
"anti aliased off
set antialias
set guifont=Monaco:h12

"set automaic intention
set smartindent
"set tabs to 4 spaces
set tabstop=4
"set shift to 4 spaces
set shiftwidth=4
"use spaces instead of tabs
set expandtab

"always numbering on
set number
"map leader i to format all and stay on the same line
:map <Leader>i mzgg=G`z

"remove trailing whitespaces
autocmd FileType c,cpp,java,php,ruby,python autocmd BufWritePre <buffer> :call <SID>StripTrailingWhitespaces()

fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun


"mark trailing whitespaces
highlight ExtraWhitespace ctermbg=grey guibg=grey
call matchadd('Todo', '\s\+$', 11)
"mark line over 80 characters
highlight OverLength ctermbg=darkred ctermfg=white guibg=#592929
call matchadd('Todo', '\%81v.\+', 12)

"NerdTree related settings

"show bookmarks on top
let NERDTreeShowBookmarks=1
"opens nerd tree if no file is specified
autocmd vimenter * if !argc() | NERDTree | endif


"Doxygen settings
let g:DoxygenToolkit_versionString="1.0"
let g:DoxygenToolkit_authorName="David Sichau <mail\"at\"sichau\"dot\"eu>"
let g:DoxygenToolkit_cinoptions="c1C1"
let g:DoxygenToolkit_licenseTag = "Copyright (c) 2013 David Sichau <mail\"at\"sichau\"dot\"eu>\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "Permission is hereby granted, free of charge, to any person obtaining a copy\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "of this software and associated documentation files (the \"Software\"), to deal\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "in the Software without restriction, including without limitation the rights\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\<enter>" 
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "copies of the Software, and to permit persons to whom the Software is\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "furnished to do so, subject to the following conditions:\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "The above copyright notice and this permission notice shall be included in\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "all copies or substantial portions of the Software.\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND EXPRESS OR\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "THE SOFTWARE.\<enter>"
