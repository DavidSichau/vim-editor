" Setup for indending
setlocal cindent
setlocal cinoptions=h1,l1,g1,t0,i4,+4,(0,w1,W4
setlocal shiftwidth=4
setlocal tabstop=4
setlocal softtabstop=4
"tabs are spaces and not tabs
setlocal expandtab
setlocal textwidth=80
setlocal wrap

"only parse the file once
"if exists("b:did_indent")
"    finish
"endif
"let b:did_indent = 1



"match every where where no or more than 1 space is at curled bracket.
call matchadd('Todo', '\S\(\| \{2,}\){', 13)
"match every where where no or more than 1 space is at normal bracket.
call matchadd('Todo', '\(for\|if\)\(\| \{2,}\)(', 13)

"match every comma with no or more than 1 space afterwards
call matchadd('Todo', '\S,\(\| \{2,}\)\S', 14)
"match using namespace
call matchadd('Todo', 'using namespace.*', 15)

" Fix up indent issues - I can't stand wasting an indent because I'm in a
" namespace.  If you don't like this then just comment this line out.
setlocal indentexpr=GoogleCppIndent()

"intent from google code style
function! GoogleCppIndent()
    let l:cline_num = line('.')

    let l:orig_indent = cindent(l:cline_num)

    if l:orig_indent == 0 | return 0 | endif

    let l:pline_num = prevnonblank(l:cline_num - 1)
    let l:pline = getline(l:pline_num)
    if l:pline =~# '^\s*template' | return l:pline_indent | endif

    " TODO: I don't know to correct it:
    " namespace test {
    " void
    " ....<-- invalid cindent pos
    "
    " void test() {
    " }
    "
    " void
    " <-- cindent pos
    if l:orig_indent != &shiftwidth | return l:orig_indent | endif

    let l:in_comment = 0
    let l:pline_num = prevnonblank(l:cline_num - 1)
    while l:pline_num > -1
        let l:pline = getline(l:pline_num)
        let l:pline_indent = indent(l:pline_num)

        if l:in_comment == 0 && l:pline =~ '^.\{-}\(/\*.\{-}\)\@<!\*/'
            let l:in_comment = 1
        elseif l:in_comment == 1
            if l:pline =~ '/\*\(.\{-}\*/\)\@!'
                let l:in_comment = 0
            endif
        elseif l:pline_indent == 0
            if l:pline !~# '\(#define\)\|\(^\s*//\)\|\(^\s*{\)'
                if l:pline =~# '^\s*namespace.*'
                    return 0
                else
                    return l:orig_indent
                endif
            elseif l:pline =~# '\\$'
                return l:orig_indent
            endif
        else
            return l:orig_indent
        endif

        let l:pline_num = prevnonblank(l:pline_num - 1)
    endwhile

    return l:orig_indent
endfunction


